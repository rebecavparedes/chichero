<!DOCTYPE html>
<html>
<head>
	<title>Dr Chichero</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/font-awesome.css">
	<meta http-equiv="X-UA-Compatible" content="ie-edge">
	<link rel="stylesheet" href="css/mystyle.css">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" a href="css/bootstrap.css"/>
	<link rel="stylesheet" a href="css/fonts.css">
	<link rel="stylesheet" href="iconos/css/fontello.css">
	<script src="js/bootstrapjquery.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/propper.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script|Lobster" rel="stylesheet">
    <script src="js/jquery-3.1.0.min.js"></script>
    <script src="js/main.js"></script>
    <link rel="icon" href="img/favicon.ico">

    <script type="text/javascript" src="js/up.js"></script>

	<script src="jsCarousel2/jquery-1.4.4.min.js" type="text/javascript"></script>

    <script src="jsCarousel2/jsCarousel-2.0.0.js" type="text/javascript"></script>

    <link href="jsCarousel2/jsCarousel-2.0.0.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        $(document).ready(function() {
            $('#carouselv').jsCarousel({ onthumbnailclick: function(src) { alert(src); }, autoscroll: true, masked: false, itemstodisplay: 3, orientation: 'v' });
            
            $('#carouselh').jsCarousel({ onthumbnailclick: function(src) { alert(src); }, autoscroll: false, circular: true, masked: false, itemstodisplay: 5, orientation: 'h' });
            $('#carouselhAuto').jsCarousel({ onthumbnailclick: function(src) { alert(src); }, autoscroll: true, masked: true, itemstodisplay: 5, orientation: 'h' });

        });       
        
    </script>

</head>
<body data-spy="scroll" data-target=".navbar">
	<div id="Header">
		<nav class="navbar navbar-expand-sm navbar-dark text-white bg-color fixed-top">
			<div class="container">
				
	<button class="navbar-toggler" data-toggle="collapse" data-target="#Navbar">
		<span class="navbar-toggler-icon"></span>
	</button>
			<a href="#" class="navbar-brand"><img class="responsive" src="img/logo.png" width="165" height="60"></a>
			<div class="collapse navbar-collapse" id="Navbar">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item"><a href="#" class="nav-link" style="color: white;">Inicio</a></li>
					<li class="nav-item"><a href="#Sobre" class="nav-link" style="color: white;">Sobre nosotros</a></li>
					<li class="nav-item"><a href="#Historia" class="nav-link" style="color: white;">Historia</a></li>
					<li class="nav-item"><a href="#Toppings" class="nav-link" style="color: white;">Toppings</a></li>
					<li class="nav-item"><a href="#Contacto" class="nav-link" style="color: white;">Contacto</a></li>
				</ul>
					<a class="icon-twitter" href="https://twitter.com/drchichero?lang=es" target="_blank"></a>
            		<a class="icon-instagram" href="https://www.instagram.com/drchichero/?hl=es-la" target="_blank"></a>
				</div>
			</div>
		</nav>
	</div>


	<div class="content-portada">
		<div class="content-item1">
            <h4>Lo natural es insuperable</h4>
        </div>
    </div>

    <div class="sobre-nosotros" >
            <h3 id="Sobre" class="sub">Sobre Nosotros</h3>
            <div class="content-details">
                <div class="content-item2">
                	<div class="col-md-12 img-1">
						<img src="img/mision.jpg">
					</div>
					<div class="p2">
					<hr/>
                    <h4>Visión</h4>
                    <p>Contemplar nuevas aperturas a nivel nacional e internacional en áreas comerciales reconocidas, brindando siempre un producto bajo una estricta formulación, con las máximas condiciones de higiene y salubridad en su elaboración y comercialización.</p>
                    <hr/>
                    </div>
                </div>
                <div class="content-item2 ">
                	<div class="p3">
                	<hr/>
                    <h4>Misión</h4>
                    <p> Satisfacer la necesidad de nuestros clientes de disfrutar un producto tan arraigado a nuestras costumbres como lo es la chicha; imponiendo un  nuevo estilo de comercialización de este producto.</p>
                    <hr/>
                    </div>
                    <div class="col-md-12 img" style="margin-top: 80px;">
						<img src="img/vision.jpg">
					</div>
                </div>
            </div>
    </div>

    <div id="Historia" class="historia">
        <div class="t-historia">
            <h3 class="sub-a">Historia</h3>
        </div>
        <div class="slideshow">
        <ul class="slider">
            <li>
                <img src="img/1.png">
            </li>
            <li>
                <img src="img/2.png">
            </li>
            <li>
                <img src="img/3.png">
            </li>
            <li>
                <img src="img/4.png">
            </li>
        </ul>   
    
        <div class="left">
            <span class="fa fa-chevron-left"></span>
        </div>

        <div class="right">
            <span class="fa fa-chevron-right"></span>
        </div>

    </div>
    </div>

 <div class="sucursales">
    <h3 class="sub">Sucursales</h3>
    <div class="box padre">
        <div id="hWrapper hijo">
                <div id="carouselh">
                    <div>
                        <img alt="" src="jsCarousel2/images/img_1.jpg" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/img_2.jpg" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/img_3.jpg" /><br />
                        <span class="thumbnail-text">Barquisimeto <br> Estado Lara</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/img_4.jpg" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/img_5.jpg" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/img_6.jpg" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/img_7.png" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/img_8.jpg" /><br />
                        <span class="thumbnail-text">Barquisimeto <br> Estado Lara</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/img_9.jpg" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/img_10.png" /><br />
                        <span class="thumbnail-text">Barquisimeto </span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/img_11.jpg" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    <div>
                        <img alt="Barquisimeto Edo Lara" src="jsCarousel2/images/img_12.jpg" /><br />
                        <span class="thumbnail-text">Barquisimeto <br> Estado Lara</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/img_13.png" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/img_14.png" /><br />
                        <span class="thumbnail-text">Acarigua <br> Estado Portuguesa</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/img_15.jpg" /><br />
                        <span class="thumbnail-text">Acarigua</span></div>
                    </div>
                </div>
        </div>
</div>  


    <div id="Toppings" class="toppings">
        <h3 class="sub-a">Toppings</h3>
        <div class="box padre">
            <div id="hWrapperAuto" class="top hijo">
                <div id="carouselhAuto">
                    <div>
                        <img alt="" src="jsCarousel2/images/1.png" /><br />
                        </div>
                    <div>
                        <img alt="" src="jsCarousel2/images/2.png" /><br />
                        </div>
                    <div>
                        <img alt="" src="jsCarousel2/images/3.png" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/4.png" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/5.png" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/6.png" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/7.png" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/8.png" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/9.png" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/10.png" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/11.png" /><br />
                        <span class="thumbnail-text">Barquisimeto </span></div>
                    <div>
                        <img alt="" src="jsCarousel2/images/12.png" /><br />
                        <span class="thumbnail-text">Barquisimeto</span></div>
                    </div>
                </div>
        </div>
    </div>

        <div class="estamos">
            <h3 class="sub">Nuestros stands</h3>
             <div class="content-module">

                <div class="content-item-e">
                    <div class="col-md-6">
                        <img src="img/sambil.jpg">
                    </div>
                    <h5> Stand C.C. Sambil</h5>
                 </div>
                
                <div class="content-item-e">
                    <div class="col-md-6">
                        <img src="img/about.jpg">
                    </div>
                    <h5> Stand C.C. Metropolis</h5>
                 </div>
                
                <div class="content-item-e">
                    <div class="col-md-6">
                        <img src="img/buena.jpg">
                    </div>
                    <h5>Stand C.C. Buenaventura</h5>
                 </div>
                
                </div>
            </div>
        </div>
            
</div>
<div id="Contacto">
<div class="myfoot">
	<h3 class="sub">Contáctanos</h3>		
	<form class="contacto" method="post" action="#">
		<div class="row">
			<div class="col-6 col-12-mobilep">
				<input type="text" name="nombre" placeholder="Tu nombre" tabindex="1" required />
			</div>
			<div class="col-6 col-12-mobilep">
				<input type="email" name="email" placeholder="Tu correo" tabindex="2" required/>
			</div>
			<div class="col-12">
				<textarea name="mensaje" placeholder="Escribe tu mensaje...." rows="6" tabindex="5" required></textarea>
			</div>
			<div class="col-12 col-12-mobilep">
                <button name="submit" type="submit" id="centro contact-submit" data-submit="...Enviar" class="button yellow medium radius"><span class="icon-warning"></span>Enviar</button>
			</div>
		</div>
	</form>
</div>
</div>

    <div class="foot">
        <div class="contentmodule">
            <div class="content-item">
                <div class="col-md-12">
                    <div class="hey">
                        <a href="#"><img src="img/logo.png" width="165" height="62"></a>
                            <div class="info">
                                <p class="white">Teléfono fijo: +58 251 232 58 38</p>
                                <p class="white">Correo: info@drchichero.com</p>
                                <p class="white">Síguenos en:</p>
                                <label class="icon-twitter"></label>
                                <label class="icon-instagram"></label>
                            </div>
                    </div>
                </div>
                    
            </div>
       
                
 <div class="content-item">
            <div class="col-md-12">
                <div >
                    <a class="twitter-timeline" data-width="220" data-height="215" href="https://twitter.com/DrChichero?ref_src=twsrc%5Etfw">Tweets by DrChichero</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
            </div>
         </div>
         <div class="content-item">
            <div class="col-md-12">
                <div>
                    <!-- InstaWidget -->
					<a href="https://instawidget.net/v/user/drchichero" id="link-e520bd06c7ce505f25cec7cb31a955f2ab7b7e7b72dfe572447a436cf54ba230">@drchichero</a>
					<script src="https://instawidget.net/js/instawidget.js?u=e520bd06c7ce505f25cec7cb31a955f2ab7b7e7b72dfe572447a436cf54ba230&width=220px"></script>
                </div>
            </div>
         </div> 
    </div>  
    </div>        
           
    <footer>
        <p>&copy; www.doctorchichero.com 2019. Todos los derechos reservados. Sitio creado por <strong><a href="http://www.branar.com">Branar C.A.</a></strong></p>
    </footer>

</body>
</html>